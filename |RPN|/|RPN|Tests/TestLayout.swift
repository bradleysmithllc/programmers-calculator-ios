//
//  TestProfile.swift
//  Programmable Calculator
//
//  Created by Bradley Smith on 11/24/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class TestLayout: JSONLayout
{
	static func load(id: String) -> Layout
	{
		if let file = Bundle(for:TestLayout.self).path(forResource: id, ofType: "json") {
			let url = URL(fileURLWithPath: file, isDirectory: false);
			
			return JSONLayoutLoader.loadFromJSON(id, url: url)!;
		}
		
		fatalError("TestLayout");
	}
}
