//
//  UnitTest.swift
//  |RPN|Tests
//
//  Created by absmiths on 7/7/20.
//  Copyright © 2020 Bradley Smith, LLC. All rights reserved.
//

import XCTest

@testable import IRPNI;

class UnitTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSplitDecimalFull() {
        let res = CalculatorCellUIView.splitDecimal("18.09");
        
        assert(res.integral == "18.");
        assert(res.decimal == "09");
    }
    
    func testSplitDecimalIntegral() {
        let res = CalculatorCellUIView.splitDecimal("18");
        
        assert(res.integral == "18");
        assert(res.decimal == "");
    }
    
    func testSplitDecimal() {
        let res = CalculatorCellUIView.splitDecimal("0.71");
        
        assert(res.integral == "0.");
        assert(res.decimal == "71");
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
