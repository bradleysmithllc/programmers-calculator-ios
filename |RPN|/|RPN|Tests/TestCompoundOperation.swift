//
//  TestProfile.swift
//  Programmable Calculator
//
//  Created by Bradley Smith on 11/24/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class TestCompoundOperation: JSONCompoundOperation
{
	init(calculator: Calculator, id: String)
	{
		if let file = Bundle(for:TestCompoundOperation.self).path(forResource: id, ofType: "json") {
			let url = URL(fileURLWithPath: file, isDirectory: false);
			
			super.init(calculator: calculator, id: id, url: url);
		}
		else
		{
			super.init(calculator: calculator);
		}
	}

	public required init(calculator: Calculator) {
	    fatalError("init(calculator:) has not been implemented")
	}
}
