//
//  IRPNIUITesting.swift
//  IRPNIUITesting
//
//  Created by Bradley Smith on 12/29/21.
//  Copyright © 2021 Bradley Smith, LLC. All rights reserved.
//

import XCTest

class IRPNIUITesting: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

	func testTaps() throws {
			// UI tests must launch the application that they test.
			let app = XCUIApplication()
			app.launch()
		
		let view = XCUIApplication().windows.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element

		let displayX = view.children(matching: .other).element(boundBy: 1)
		let displayY = view.children(matching: .other).element(boundBy: 0)

		let element9 = view.children(matching: .other).element(boundBy: 12)
		let element8 = view.children(matching: .other).element(boundBy: 11)
		let element7 = view.children(matching: .other).element(boundBy: 10)
		let element4 = view.children(matching: .other).element(boundBy: 14)
		let element5 = view.children(matching: .other).element(boundBy: 15)
		let element6 = view.children(matching: .other).element(boundBy: 16)
		let element1 = view.children(matching: .other).element(boundBy: 18)
		let element2 = view.children(matching: .other).element(boundBy: 19)
		let element3 = view.children(matching: .other).element(boundBy: 20)
		let element0 = view.children(matching: .other).element(boundBy: 22)
		let elementEnt = view.children(matching: .other).element(boundBy: 24)

		element1.tap()
		element2.tap()
		element3.tap()
		element4.tap()
		element5.tap()
		element6.tap()
		element7.tap()
		element8.tap()
		element9.tap()
		element0.tap()
		elementEnt.tap()
		
		element1.press(forDuration: 2, thenDragTo: element9)
	}

	func testLongPress() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
			
			let view = XCUIApplication().windows.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
			let element9 = view.children(matching: .other).element(boundBy: 12)
			let element8 = view.children(matching: .other).element(boundBy: 11)
			let element7 = view.children(matching: .other).element(boundBy: 10)
			let element4 = view.children(matching: .other).element(boundBy: 14)
			let element5 = view.children(matching: .other).element(boundBy: 15)
			let element6 = view.children(matching: .other).element(boundBy: 16)
			let element1 = view.children(matching: .other).element(boundBy: 18)
			let element2 = view.children(matching: .other).element(boundBy: 19)
			let element3 = view.children(matching: .other).element(boundBy: 20)
			let element0 = view.children(matching: .other).element(boundBy: 22)
			let elementEnt = view.children(matching: .other).element(boundBy: 24)

			element1.press(forDuration: 10)
			element2.press(forDuration: 10)
			element3.press(forDuration: 10)
			element4.press(forDuration: 10)
			element5.press(forDuration: 10)
			element6.press(forDuration: 10)
			element7.press(forDuration: 10)
			element8.press(forDuration: 10)
			element9.press(forDuration: 10)
			element0.press(forDuration: 10)
			elementEnt.press(forDuration: 10)
    }

	func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
					let element = XCUIApplication().windows.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
