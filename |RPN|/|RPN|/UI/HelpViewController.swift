//
//  ViewController.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit

import WebKit

open class HelpViewController: UIViewController
{
		@IBOutlet var wkWebView: WKWebView!;

    open override func viewWillAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated);
        
		var layout: String = "";
		
		let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
		let shortVersion = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as! String
		
		let activeLayer = AppDelegate.configuration.activeLayer;

		let l = AppDelegate.configuration.activeLayout;

		layout = l.generateHtmlTable(activeLayer, formatter: HelpFormatter(profile: AppDelegate.configuration.activeProfile));
		
		print("\(layout)");

		/*
		layout = "<table cellspacing=\"0\"><tbody>";

		for row in 0..<l.rows()
		{
			layout += "<tr>";

			for col in 0..<l.columns()
			{
				let cell = l.cell(row: row, column: col);

				if (cell.cellFunction(activeLayer) == .operation)
				{
					let pcolor = AppDelegate.configuration.activeProfile.buttonColor(buttonSelector: cell.profileKey(activeLayer), layer: activeLayer);
					let mcolor = AppDelegate.configuration.activeProfile.activeMnemonicColor(buttonSelector: cell.profileKey(activeLayer), layer: activeLayer);

					layout += "<td style=\"background: rgb(\(pcolor.red), \(pcolor.green), \(pcolor.blue))\">";

					if let op = cell.operation(activeLayer)
					{
						layout += "<a href=\"#\(op.id())\"><span style=\"color: rgb(\(mcolor.red), \(mcolor.green), \(mcolor.blue))\">" + op.decoratedMnemonic() + "</span></a>";
					}
					else if cell.profileKey(activeLayer) == "settings"
					{
						layout += "info";
					}

					layout += "</td>";
				}
				else
				{
					let pcolor = AppDelegate.configuration.activeProfile.buttonColor(buttonSelector: cell.profileKey(activeLayer), layer: activeLayer);

					layout += "<td style=\"background: rgb(\(pcolor.red), \(pcolor.green), \(pcolor.blue))\">&nbsp;</td>";
				}
			}

			layout += "</tr>"
		}

		layout += "</tbody></table>";
		*/

		var operations = "<table id=\"operations\"  cellspacing=\"0\"><thead><th>Operation ID</th><th>Mnemonic</th><th>Description</th><th>Stack</th><th>Formula</th></thead><tbody>";

		let coperations = AppDelegate.calculator.operations();

		for (index, id) in AppDelegate.calculator.operationIds().enumerated()
		{
			let op = coperations[id]!;
			let formula: String? = op.formula();

			let clname = index % 2 == 0 ? "evenTableRow" : "oddTableRow";

			let mneText = op.decoratedMnemonic();
			
			operations += "<tr class=\"\(clname)\">";
			operations += "<td>";
			operations += id
			operations += "</td>";
			operations += "<td>";
			operations += "<a name=\"\(id)\">"
			operations += mneText
			operations += "</a>"
			operations += "</td>";
			operations += "<td>";
			operations += op.description()
			operations += "</td>";
			operations += "<td>";
			
			if let se = op.stackAffect()
			{
				for (index, effect) in se.enumerated()
				{
					if (index != 0)
					{
						operations += ",";
					}

					switch (effect)
					{
					case .Pop:
						operations += "Pop"
					case .Push:
						operations += "Push"
					case .Peek:
						operations += "Peek"
					}
				}
			}

			operations += "</td>";
			operations += "<td>";
			operations += (formula == nil ? "-" : formula!)
			operations += "</td>";
			operations += "</tr>";
		}
		operations += "</tbody></table>";

		let URLWithString: URL = AppDelegate.configuration.urlAtPath(["help.html"]);
		
		do
		{
			var template = try String(NSString(contentsOf: URLWithString, encoding: String.Encoding.utf8.rawValue));
			
			template = template.replacingOccurrences(of: "%short-version%", with: shortVersion)
			template = template.replacingOccurrences(of: "%version%", with: version)
			template = template.replacingOccurrences(of: "%layout%", with: layout)
			template = template.replacingOccurrences(of: "%operations%", with: operations)
			
			// process meta-tags
			let html = HelpViewController.processTags(template);

            wkWebView.loadHTMLString(html, baseURL: nil);
            //if let url = URL(string: "https://www.apple.com") {
            //            let req = URLRequest(url: url)
            //            webView.load(req)
            //        }
        }
		catch
		{
            NSLog("Failure")
		}
	}
	
	public static func processTags(_ html: String) -> String
	{
		var newHtml = html;
		
		for tag in HelpViewController.metaTags
		{
			newHtml = newHtml.replacingOccurrences(of: tag.open.tag, with: tag.open.replaceWith);
			
			if let replClose = tag.close
			{
				newHtml = newHtml.replacingOccurrences(of: replClose.tag, with: replClose.replaceWith);
			}
		}
		
		return newHtml;
	}

	fileprivate static let metaTags: [(open: (tag: String, replaceWith: String), close: (tag: String, replaceWith: String)?)] = [
		(open: (tag: "<math>", replaceWith: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">"), close: nil),
		(open: (tag: "<stack-x />", replaceWith: "<span class=\"stack-x\">x</span>"), close: nil),
		(open: (tag: "<stack-y />", replaceWith: "<span class=\"stack-y\">y</span>"), close: nil),
		(open: (tag: "<primary-identifier>", replaceWith: "<span class=\"primaryIdentifier\">"), close: (tag: "</primary-identifier>", replaceWith: "</span>")),
		(open: (tag: "<dependent-identifier>", replaceWith: "<span class=\"dependentIdentifier\">"), close: (tag: "</dependent-identifier>", replaceWith: "</span>")),
		(open: (tag: "<operation>", replaceWith: "<span class=\"operation\">"), close: (tag: "</operation>", replaceWith: "</span>"))
	];
}

class HelpFormatter : NullHtmlFormatter
{
	let profile: Profile;

	init(profile: Profile)
	{
		self.profile = profile;
	}
	
	override func tableAttributes() -> String
	{
		return " cellspacing=\"0\" style=\"width: 100%; height: 20em\"";
	}
	
	override func cellText(_ cell: CalculatorCell, layer: Layer) -> String
	{
		switch (cell.cellFunction(layer))
		{
		case .function:
			switch(cell.uiFunctionType(layer))
			{
			case .settings:
				return "i{!}"
			case .function_a:
				return "2{nd}"
			case .function_b:
				return "3{rd}"
			case .function_c:
				return "4{th}"
			case .function_d:
				return "5{th}"
			case .normal:
				return "↩";
			case .display_t:
				return ":T";
			case .display_z:
				return ":Z";
			case .display_y:
				return ":Y";
			case .display_x:
				return ":X";
			case .placeholder:
				return "";
			}
		case .operation:
			let mcolor = profile.activeMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);

			if let op = cell.operation(layer)
			{
				return "<a href=\"#\(op.id())\"><span style=\"color: rgb(\(mcolor.redInt), \(mcolor.greenInt), \(mcolor.blueInt))\">" + op.decoratedMnemonic() + "</span></a>";
			}
		}
		
		return "";
	}

	override func tdAttributes(_ cell: CalculatorCell, layer: Layer) -> String
	{
		var attr = "";

		if (cell.cellFunction(layer) == .operation)
		{
			let pcolor = profile.buttonColor(buttonSelector: cell.profileKey(layer), layer: layer);
			
			attr = " style=\"background-color: rgb(\(pcolor.redInt), \(pcolor.greenInt), \(pcolor.blueInt))\"";
			
		}
		else
		{
			let pcolor = profile.buttonColor(buttonSelector: cell.profileKey(layer), layer: layer);
			
			attr = " style=\"background-color: rgb(\(pcolor.redInt), \(pcolor.greenInt), \(pcolor.blueInt))\"";
		}

		return attr;
	}
}
