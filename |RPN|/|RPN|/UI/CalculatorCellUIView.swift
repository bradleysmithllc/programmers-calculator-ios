//
//  CalculatorCellUIView.swift
//  |RPN|
//
//  Created by Bradley Smith on 2/3/16.
//  Copyright © 2016 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit
import programmers_calculator_core
import os

protocol CellUIView
{
	func drawInContext(rect: CGRect, context: CGContext);
}

open class CalculatorCellUIView : UIView, CellUIView
{
	var cellText: String? = nil;

	private var _superUIView: CalculatorUIView?;
	private var _cell: CalculatorCell?;

	private var touchingMe = false;
	private var attributedStringCache = [String: (attributedString: NSAttributedString, boundsRect: CGRect)]();
	
	let log: OSLog = OSLog.init(subsystem: "CalculatorCellUIView", category: String(describing: type(of: self)));

	var cellAllowsInteractionDuringRunningOperation: Bool
	{
		get
		{
			return false;
		}
	};
	
	var debugGraphics: Bool
	{
			return Configuration._debugGraphics;
	}

	var superUIView: CalculatorUIView
	{
		get
		{
			return _superUIView!;
		}
	}
	
	var calculatorCell: CalculatorCell
	{
		get
		{
			return _cell!;
		}
	}
	
	var activeLayer: Layer
		{
		get
		{
			return superUIView.configuration.activeLayer;
		}
	}
	
	var configuration: Configuration
		{
		get
		{
			return superUIView.configuration;
		}
	}
	
	var calculator: Calculator
		{
		get
		{
			return superUIView.calculator;
		}
	}
	
	var activeProfile: Profile
		{
		get
		{
			return superUIView.activeProfile;
		}
	}
	
	var activeLayout: Layout
		{
		get
		{
			return superUIView.configuration.activeLayout;
		}
	}
	
	final func setUp(_ superView: CalculatorUIView, cell: CalculatorCell)
	{
		self._superUIView = superView;
		self._cell = cell;
	}

	public static func splitDecimal(_ input: String) -> (integral: String, decimal: String)
	{
		let decimalPortion: String;
		let integralPortion: String;
		
		// remove decimal portion
		if let srange = input.range(of: ".")
		{
			// strip decimals
            decimalPortion = String(input[input.index(srange.lowerBound, offsetBy: 1)...]);
			
            integralPortion = String(input[...srange.lowerBound]);
		}
		else
		{
			decimalPortion = "";
			integralPortion = input;
		}
		
		return (integral: integralPortion, decimal: decimalPortion);
	}
	
	public static func separatize(_ input: String, digitsPerSeparator: Int) -> String
	{
		var result: String = "";
		
		for (index, character) in input.reversed().enumerated()
		{
			if (character != "-" && index != 0 && index % digitsPerSeparator == 0)
			{
				result.append(Character(","));
			}
			
			result.append(character);
		}
		
		return String(result.reversed());
	}
	
	static func makeNormalFacetTextRect(_ cellRect: CGRect) -> CGRect
	{
		// new origin
		let new_x = cellRect.origin.y + cellRect.size.width * UILayoutConstants.NORMAL_FACET_LEFT_MARGIN;
		let new_y = cellRect.origin.x + UILayoutConstants.NORMAL_FACET_TOP_BAR_PERCENT * cellRect.size.height;
		
		// new width.  subtract the two margins from the width
		let new_width = cellRect.size.width * (1.0 - (UILayoutConstants.NORMAL_FACET_LEFT_MARGIN + UILayoutConstants.NORMAL_FACET_RIGHT_MARGIN));
		
		// new height.  subtract the two margins from the height
		let new_height = cellRect.size.height * (1.0 - (UILayoutConstants.NORMAL_FACET_TOP_BAR_PERCENT + UILayoutConstants.NORMAL_FACET_BOTTOM_PERCENT));
		
		return CGRect(x: new_x, y: new_y, width: new_width, height: new_height).integral;
	}
	
	public static func displayNumber(_ number: NSDecimalNumber, calculator: Calculator) -> String
	{
		return number.formattedNumberInBase(
			numericBase: calculator.stack().getNumericBase(),
			numericBehavior: calculator.stack().getNumericBehavior(),
			applySeparator: true
		);
		//return CalculatorCellOperationUIView.displayNumber(
		//	number,
		//	numericBehavior: calculator.stack().getNumericBehavior(),
		//	integerWordSize: calculator.stack().getIntegerWordSize(),
		//	numericBase: calculator.stack().getNumericBase(),
		//	decimals: calculator.stack().getDecimals());
		//return "";
	}
	
	public static func displayNumber1(_ number: NSDecimalNumber, numericBehavior: NumericBehavior, integerWordSize: IntegerWordSize?, numericBase: NumericBase, decimals: UInt8) -> String
	{
		//let disp: NSDecimalNumber;
		
		//if (numericBase == NumericBase.base10)
		//{
			 // round to required decimals
		//	disp = number.decimalNumberRoundedToDigits(withDecimals: decimals);
		//}
		//else
		//{
		//	disp = number;
		//}

		// return prepareNumber1(disp.formattedNumberInBase(numericBase: numericBase, unsigned: numericBehavior == .UnsignedInteger), numericBase: numericBase);
		return "";
	}

	public static func prepareNumber1(_ formatted: String, numericBase: NumericBase) -> String
	{
		// add commas
		//var digitsToSeparate: Int;

		//switch (numericBase) {
		//	case .base2:
		//		digitsToSeparate = 4;
		//	case .base8:
		//		digitsToSeparate = 4;
		//	case .base10:
		//		digitsToSeparate = 3;
		//	case .base16:
		//		digitsToSeparate = 4;
		//}
		
		//let splitString = CalculatorCellOperationUIView.splitDecimal(formatted);
		
		//let sepString = CalculatorCellOperationUIView.separatize(splitString.integral, digitsPerSeparator: digitsToSeparate) + splitString.decimal;
        
		//return sepString;
		return "";
	}

	open func moderatedTouchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {}
	
	final override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (!superUIView.runningOperation || cellAllowsInteractionDuringRunningOperation)
		{
			touchingMe = true;
			moderatedTouchesBegan(touches, with: event);
		}
	}
	
	open func moderatedTouchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {}
	
	final override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if let touch = touches.first {
			print("\(touch.force) - \(touch.maximumPossibleForce)");
		}
				
		if (!superUIView.runningOperation || cellAllowsInteractionDuringRunningOperation)
		{
			moderatedTouchesMoved(touches, with: event);
		}
	}
	
	open func moderatedTouchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {}
	
	final override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (!superUIView.runningOperation || cellAllowsInteractionDuringRunningOperation)
		{
			touchingMe = false;
			moderatedTouchesEnded(touches, with: event);
		}
	}
	
	open func moderatedTouchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {}
	
	final override public func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (!superUIView.runningOperation || cellAllowsInteractionDuringRunningOperation)
		{
			touchingMe = false;
			moderatedTouchesCancelled(touches, with: event);
		}
	}
	
	func drawInContext(rect: CGRect, context: CGContext)
	{
		preconditionFailure("This method must be overridden")
	}

	public final override func draw(_ rect: CGRect)
	{
		guard let context = UIGraphicsGetCurrentContext() else {
			return;
		}
		
		context.saveGState();
		
		drawInContext(rect: rect, context: context);
		
		if (debugGraphics)
		{
			// draw bounding box for reference
			// upper-left
			context.setLineWidth(1.0);
			
			context.setStrokeColor(UIColor.red.cgColor);
			context.move(to: CGPoint(x: 10, y: 0));
			context.addLine(to: CGPoint(x: 0, y: 0));
			context.addLine(to: CGPoint(x: 0, y: 10));
			
			context.strokePath();
			
			// upper-right
			context.setStrokeColor(UIColor.blue.cgColor);
			context.move(to: CGPoint(x: rect.width, y: 10));
			context.addLine(to: CGPoint(x: rect.width, y: 0));
			context.addLine(to: CGPoint(x: rect.width - 10, y: 0));
			
			context.strokePath();
			
			// lower-right
			context.setStrokeColor(UIColor.green.cgColor);
			context.move(to: CGPoint(x: rect.width, y: rect.height - 10));
			context.addLine(to: CGPoint(x: rect.width, y: rect.height));
			context.addLine(to: CGPoint(x: rect.width - 10, y: rect.height));
			
			context.strokePath();
			
			// lower-left
			context.setStrokeColor(UIColor.black.cgColor);
			context.move(to: CGPoint(x: 10, y: rect.height));
			context.addLine(to: CGPoint(x: 0, y: rect.height));
			context.addLine(to: CGPoint(x: 0, y: rect.height - 10));
			
			context.strokePath();
			
			// draw edgelines
			// left
			context.setStrokeColor(UIColor.purple.cgColor);
			context.move(to: CGPoint(x: 5, y: 1));
			context.addLine(to: CGPoint(x: 5, y: rect.height - 1));
			
			context.strokePath();
			
			// top
			context.setStrokeColor(UIColor.orange.cgColor);
			context.move(to: CGPoint(x: 1, y: 5));
			context.addLine(to: CGPoint(x: rect.width - 1, y: 5));
			
			context.strokePath();
			
			// right
			context.setStrokeColor(UIColor.magenta.cgColor);
			context.move(to: CGPoint(x: rect.width - 5, y: 1));
			context.addLine(to: CGPoint(x: rect.width - 5, y: rect.height - 1));
			
			context.strokePath();
			
			// bottom
			context.setStrokeColor(UIColor.darkGray.cgColor);
			context.move(to: CGPoint(x: rect.width - 1, y: rect.height - 5));
			context.addLine(to: CGPoint(x: 1, y: rect.height - 5));
			
			context.strokePath();
		}
		
		context.restoreGState();
	}
	
	func drawCellBackground(
		_ context: CGContext,
		cellRect: CGRect,
		cell: CalculatorCell,
		touching: Bool,
		operation: programmers_calculator_core.Operation?
		)
	{
		let layer: Layer = configuration.activeLayer;
		
		// draw the outline around the box
		let clr: UIColor;
		
		if (touching)
		{
			// special case for displays
			if cell.cellFunction(layer) == function.function
			{
				if
					cell.uiFunctionType(layer) == .display_x ||
						cell.uiFunctionType(layer) == .display_y ||
						cell.uiFunctionType(layer) == .display_z ||
						cell.uiFunctionType(layer) == .display_t
				{
					clr = activeProfile.buttonColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
				else
				{
					clr = activeProfile.buttonPressedColor(buttonSelector: cell.profileKey(layer), layer: layer);
				}
			}
			else
			{
				clr = activeProfile.buttonPressedColor(buttonSelector: cell.profileKey(layer), layer: layer);
			}
		}
		else
		{
			clr = activeProfile.buttonColor(buttonSelector: cell.profileKey(layer), layer: layer);
		}
		
		context.setFillColor(clr.cgColor);
		context.fill(cellRect);
	}

	func drawCellBorder(context: CGContext, cellRect: CGRect, cell: CalculatorCell, touching: Bool, operation: programmers_calculator_core.Operation?)
	{
		let layer: Layer = activeLayer;
		
		context.setLineWidth(1.0);
		
		context.setStrokeColor(activeProfile.lineColor(
			buttonSelector: cell.profileKey(layer),
			layer: layer
			).cgColor
		);

		context.move(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y));
		context.addLine(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y));
		context.move(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y));
		context.addLine(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y + cellRect.size.height));
		context.move(to: CGPoint(x: cellRect.origin.x + cellRect.size.width, y: cellRect.origin.y + cellRect.size.height));
		context.addLine(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y + cellRect.size.height));
		context.move(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y + cellRect.size.height));
		context.addLine(to: CGPoint(x: cellRect.origin.x, y: cellRect.origin.y));
		
		context.strokePath();
	}

	func drawCellText(context: CGContext, cellRect: CGRect, cell: CalculatorCell, touching: Bool, operation: programmers_calculator_core.Operation?)
	{
		context.saveGState();
		let layer: Layer = activeLayer;
		let buttonType = cell.profileKey(layer);
		let font: UIFont;
		let mne: String?;
		
		let clr: UIColor;
		
		var state = 0;
		
		switch(cell.cellFunction(layer))
		{
		case .operation:
			// check for operation
			if let _operation = operation
			{
				state |= 2;
					
				mne = _operation.mnemonic();

				if (_operation.validateStack())
				{
					state |= 8;
					font = activeProfile.activeMnemonicFont(buttonSelector: buttonType, layer: layer)

					if (touching)
					{
						state |= 1;
						clr = activeProfile.activeMnemonicColorPressed(buttonSelector: cell.profileKey(layer), layer: layer);
					}
					else
					{
						state |= 4;
						clr = activeProfile.activeMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
					}
				}
				else
				{
					state |= 16;
					clr = activeProfile.inactiveMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
					font = activeProfile.inactiveMnemonicFont(buttonSelector: buttonType, layer: layer)
				}
			}
			else
			{
				mne = "INV";
				clr = activeProfile.inactiveMnemonicColor(buttonSelector: cell.profileKey(layer), layer: layer);
				font = activeProfile.inactiveMnemonicFont(buttonSelector: buttonType, layer: layer)
			}

			break;
		case .function:
			state |= 32;
			
			switch(cell.uiFunctionType(layer))
			{
			case .normal:
				state |= 64;
				mne = String("↩︎");
				font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.normal.rawValue, layer: layer)
				clr = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.normal.rawValue, layer: layer);
				break;
			case .settings:
				state |= 512;
				mne = "i {!}";
				font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.settings.rawValue, layer: layer)
				clr = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.settings.rawValue, layer: layer);
				break;
			case .function_a:
				state |= 1024;
				mne = "2{nd}";
				font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.function_a.rawValue, layer: layer)
				clr = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.function_a.rawValue, layer: layer);
				break;
			case .function_b:
				state |= 2048;
				mne = "3{rd}";
				font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.function_b.rawValue, layer: layer)
				clr = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.function_b.rawValue, layer: layer);
				break;
			case .function_c:
				state |= 4096;
				mne = "4{th}";
				font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.function_c.rawValue, layer: layer)
				clr = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.function_c.rawValue, layer: layer);
				break;
			case .function_d:
				state |= 16384;
				mne = "5{th}";
				font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.function_d.rawValue, layer: layer)
				clr = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.function_d.rawValue, layer: layer);
				break;
			case .placeholder:
				state |= 32768;
				mne = "INV";
				font = activeProfile.activeMnemonicFont(buttonSelector: UIFunction.placeholder.rawValue, layer: layer)
				clr = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.placeholder.rawValue, layer: layer);
				break;
			default:
				mne = "INV";
				fatalError();
			}
			break;
		}
		
		context.setStrokeColor(clr.cgColor);
		
		// overlay the mnemonic
		if let textToDraw = mne
		{
			cellText = textToDraw
			var textRect = CalculatorCellOperationUIView.makeNormalFacetTextRect(cellRect);
			
			// reset the coordinates to 0 for each cell
			let tx = textRect.origin.x;
			let ty = textRect.origin.y;
			
			context.clip(to: textRect);
			context.translateBy(x: tx, y: ty);
			
			textRect.origin.x = 0;
			textRect.origin.y = 0;
			
			drawText(
				context, text: textToDraw, textColor: clr, targetRect: textRect, useCache: true, pressed: touching, alignment: .center, state: state, font: font
			);
		}
		
		context.restoreGState();
	}
	
	func drawText(
		_ context: CGContext, text: String, textColor: UIColor, targetRect: CGRect, useCache: Bool, pressed: Bool, alignment: AlignPosition, state: Int, font: UIFont
		)
	{
		if text.trimmingCharacters(in: CharacterSet.whitespaces) == ""
		{
			return;
		}
		
		let key: String = makeKey(text + "." + pressed.description + "." + String(state));
		
		var strContext: (attributedString: NSAttributedString, boundsRect: CGRect);
		
		let stringContext = attributedStringCache[key];
		
		if useCache && stringContext != nil
		{
			strContext = stringContext!;
		}
		else
		{
			strContext = TextDrawing.makeAttributedStringForBounds(string: text, font: font, viewRect: targetRect, textColor: textColor);
		}
		
		TextDrawing.drawTextCentered(context: context, attributedString: strContext.attributedString, viewRect: targetRect, textRect: strContext.boundsRect, alignment: alignment);
		
		if (useCache)
		{
			attributedStringCache[key] = strContext;
		}
	}
	
	func makeKey(_ text: String) -> String
	{
		return text + "." + activeProfile.id() + "." + activeLayout.id() + "." + (configuration.isLandscape() ? "Landscape" : "Portrait");
	}
}
