//
//  BaseDescribed.swift
//  Programmable Calculator
//
//  Created by Bradley Smith on 11/24/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class BaseDescribed: Described
{
	fileprivate let _id: String;
	fileprivate let _description: String;
	fileprivate let _name: String;
	
	init(id: String, description: String, name: String)
	{
		_id = id;
		_description = description;
		_name = name;
	}

	open func id() -> String
	{
		return _id;
	}
	
	open func name() -> String
	{
		return _name;
	}
	
	open func description() -> String
	{
		return _description;
	}
}
