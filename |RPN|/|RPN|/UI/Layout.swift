//
//  Perspective.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

import programmers_calculator_core

public enum Layer: String
{
	case normal = "normal";
	case function_a = "function-a";
	case function_b = "function-b";
	case function_c = "function-c";
	case function_d = "function-d";

	public static let allValues = [normal, function_a, function_b, function_c, function_d];
	public static let mapValues = [normal.rawValue: normal, function_a.rawValue: function_a, function_b.rawValue: function_b, function_c.rawValue: function_c, function_d.rawValue: function_d];
}

public enum UIFunction: String
{
	case display_x = "display-x";
	case display_y = "display-y";
	case display_z = "display-z";
	case display_t = "display-t";

	case normal = "normal";
	case settings = "settings";
	case function_a = "function-a";
	case function_b = "function-b";
	case function_c = "function-c";
	case function_d = "function-d";
	case placeholder = "placeholder";
	
	public static let allValues = [display_x, display_y, display_z, display_t, normal, function_a, function_b, function_c, function_d, settings, placeholder];
	public static let mapValues = [display_x.rawValue: display_x, display_y.rawValue: display_y, display_z.rawValue: display_z, display_t.rawValue: display_t, normal.rawValue: normal, function_a.rawValue: function_a, function_b.rawValue: function_b, function_c.rawValue: function_c, function_d.rawValue: function_d, placeholder.rawValue: placeholder, settings.rawValue: settings];
}

public enum Orientation: String
{
	case landscape = "landscape";
	case portrait = "portrait";
	
	public static let allValues = [landscape, portrait];
	public static let mapValues = [landscape.rawValue: landscape, portrait.rawValue: portrait];
}

public enum function : String
{
	case operation;
	case function;
}

public protocol CalculatorCell
{
	func cellKey() -> String

	func layers() -> [Layer];
	
	func cellFunction(_ layer: Layer) -> function;
	func overrideOperationType(_ layer: Layer) -> OperationType?;
	func operation(_ layer: Layer) -> programmers_calculator_core.Operation?;

	func profileKey(_ layer: Layer) -> String;

	func uiFunctionType(_ layer: Layer) -> UIFunction;

	// New
	func columnSpan() -> UInt8;
	func rowSpan() -> UInt8;
	func primalRow() -> UInt8
	func primalColumn() -> UInt8
}

public protocol Layout: Described
{
	func layers() -> [Layer]

	/** Return an array of numeric bases this perspective can support.  empty for any.
	  * This is used when E.G. a perspective does not have operands to support the base,
	  * such as a binary perspective which only contains 0 and 1 operands.
		*/
	func supportedNumericBases() -> [NumericBase]?
	
	/** Return an array of integer behaviors this perspective can support.  empty for any.
	*/
	func supportedNumericBehaviors() -> [NumericBehavior]?
	
	/** Return an array of integer word sizes this perspective can support.  empty for any.
	*/
	func supportedIntegerWordSizes() -> [IntegerWordSize]?

	/**  Ideal number of decimal points in this perspective.
	*/
	func suggestedDecimals() -> UInt8?

	func suggestedOrientation() -> Orientation;

	func rows() -> UInt8;
	func columns() -> UInt8;

	func rowWeights() -> [UInt8];
	func columnWeights() -> [UInt8];

	func cells() -> [CalculatorCell];
	func cell(row: UInt8, column: UInt8) -> CalculatorCell;

	/*Given the current state of the calculator, ensure that
	  it is compatible with this layout.  E.G., if the numeric mode is decimal but
		the current layout only operates with unsigned integers.  Change state as neccessary.
	*/
	func enforceConsistency(_ calculator: Calculator);

	func generateHtmlTable(_ layer: Layer, formatter: HtmlFormatter?) -> String;
	func populateOperations(_ calculator: Calculator);
}

public protocol HtmlFormatter
{
	func prefix() -> String;
	func postfix() -> String;
	func tableAttributes() -> String;
	func tdAttributes(_ cell: CalculatorCell, layer: Layer) -> String;
	func cellText(_ cell: CalculatorCell, layer: Layer) -> String;
}

class NullHtmlFormatter : HtmlFormatter
{
	func prefix() -> String
	{
		return "";
	}

	func postfix() -> String
	{
		return "";
	}

	func tableAttributes() -> String
	{
		return "";
	}
	
	func tdAttributes(_ cell: CalculatorCell, layer: Layer) -> String
	{
		return "";
	}

	func cellText(_ cell: CalculatorCell, layer: Layer) -> String
	{
		return "";
	}
}
