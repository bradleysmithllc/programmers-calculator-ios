//
//  CalculatorGridView.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/23/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit;

import programmers_calculator_core

open class CalculatorCellDisplayFunctionUIView: CalculatorCellUIView
{
	var touching: Bool = false;
	var touchingStartX: CGFloat = 0.0;
	var mathHub: MathHub = MathHub.mathHub();
	
	fileprivate var attributedStringCache = [String: (attributedString: NSAttributedString, boundsRect: CGRect)]();
	
	fileprivate static var checkCount: Int = 0;
	
	override func drawInContext(rect: CGRect, context: CGContext)
	{
		let conf = configuration;

		drawCellBackground(
			context,
			cellRect: rect,
			cell: calculatorCell,
			touching: touching,
			operation: calculatorCell.operation(conf.activeLayer)
		);
		
		drawCellBorder(context: context, cellRect: rect, cell: calculatorCell, touching: touching, operation: calculatorCell.operation(conf.activeLayer));
		
		if calculatorCell.uiFunctionType(activeLayer) == .display_x
		{
			drawDisplayX(context, cellRect: rect);
		}
		else if calculatorCell.uiFunctionType(activeLayer) == .display_y
		{
			// something to do?
			if (calculator.stack().size() > 1)
			{
				drawDisplayN(context, cellRect: rect, element: calculator.stack().getYElement());
			}
		}
		else if calculatorCell.uiFunctionType(activeLayer) == .display_z
		{
			// something to do?
			if (calculator.stack().size() > 2)
			{
				drawDisplayN(context, cellRect: rect, element: calculator.stack().getZElement());
			}
		}
		else if calculatorCell.uiFunctionType(activeLayer) == .display_t
		{
			// something to do?
			if (calculator.stack().size() > 3)
			{
				drawDisplayN(context, cellRect: rect, element: calculator.stack().getTElement());
			}
		}
		else
		{
			drawCellText(context: context, cellRect: rect, cell: calculatorCell, touching: touching, operation: calculatorCell.operation(activeLayer));
		}
	}
	
	fileprivate func drawDisplayX(_ context: CGContext, cellRect: CGRect)
	{
		var clr: UIColor = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.display_x.rawValue, layer: activeLayer);
		
		let lastMessage: (msg: String, failure: Bool)! = mathHub.lastMessage();
		
		if (lastMessage != nil && lastMessage.failure)
		{
			clr = UIColor.brown;
		}
		
		context.setStrokeColor(clr.cgColor);
		
		// overlay the mnemonic
		let mne: String = CalculatorCellDisplayFunctionUIView.getDisplayXText(calculator);
		
		var textRect = cellRect;
			
		// reset the coordinates to 0 for each cell
		let tx = textRect.origin.x - superUIView.displayOffset;
		let ty = textRect.origin.y;
			
		context.clip(to: textRect);
		context.translateBy(x: tx, y: ty);
			
		textRect.origin.x = 0;
		textRect.origin.y = 0;
			
		drawText(context, text: mne, textColor: clr, targetRect: textRect, useCache: false, pressed: false, alignment: .right, state: 0,
			font: activeProfile.activeMnemonicFont(buttonSelector: UIFunction.display_x.rawValue, layer: activeLayer)
		);
	}
	
	fileprivate func drawDisplayN(_ context: CGContext, cellRect: CGRect, element: StackElement)
	{
		let clr: UIColor = activeProfile.activeMnemonicColor(buttonSelector: UIFunction.display_y.rawValue, layer: activeLayer);
		
		context.setStrokeColor(clr.cgColor);
		
		let textToDraw = element.isMessage ? element.message() : displayNumber(element.number());
		
		var textRect = cellRect;
		
		// reset the coordinates to 0 for each cell
		let tx = textRect.origin.x - superUIView.displayOffset;
		let ty = textRect.origin.y;
		
		context.clip(to: textRect);
		context.translateBy(x: tx, y: ty);
		
		textRect.origin.x = 0;
		textRect.origin.y = 0;
		
		drawText(context, text: textToDraw, textColor: clr, targetRect: textRect, useCache: false, pressed: false, alignment: .right, state: 0,
			font: activeProfile.activeMnemonicFont(buttonSelector: UIFunction.display_y.rawValue, layer: activeLayer)
		);
	}
	
	public static func getDisplayXText(_ calculator: Calculator) -> String
	{
		let stack = calculator.stack();

		if (stack.inNumericEntry())
		{
			let image = stack.currentInputBuffer().progressTextBuffer();

			return image;
		}
		else
		{
			// grab X-display
			let sxe = calculator.stack().getXElement();
			let mne = sxe.isMessage ? sxe.message() : displayNumber(sxe.number(), calculator: calculator);
			
			return mne;
		}
	}

	func displayNumber(_ number: NSDecimalNumber) -> String
	{
		return CalculatorCellDisplayFunctionUIView.displayNumber(number, calculator: calculator);
	}
	
	open override func moderatedTouchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = true;
		
		if let x = touches.first?.location(in: self).x
		{
			touchingStartX = x;
		}
		
		setNeedsDisplay();
	}
	
	open override func moderatedTouchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if let x = touches.first?.location(in: self).x
		{
			if calculatorCell.uiFunctionType(activeLayer) == .display_x
            {
                superUIView.displayOffset = max(touchingStartX - x, 0.0);
            }
		}
		
		setNeedsDisplay();
		//superUIView.redrawDisplays();
	}
	
	open override func moderatedTouchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		let moved: Bool;
		
		if let x = touches.first?.location(in: self).x
		{
			if (touchingStartX - x) > (bounds.width / 2.0)
			{
				moved = true;
			}
			else
			{
				moved = false;
			}
		}
		else
		{
			moved = false;
		}
		
		superUIView.displayOffset = 0.0;
		touching = false;
		
		let layer = activeLayer;
		
		switch (calculatorCell.uiFunctionType(layer))
		{
			// if there are two touches on a display cell, then this is a history rewind
		case .display_x:
			if moved
			{
				// doesn't matter where . . .
				calculator.stack().revertToCheckPoint();
				// animate
				superUIView.animate(100, endPoint: Int(bounds.width));
				superUIView.redrawSubviews()
			}
			superUIView.redrawSubviews();
		case .display_y:
			if false && moved
			{
				// doesn't matter where . . .
				calculator.stack().revertToCheckPoint();
				superUIView.animate(100, endPoint: Int(bounds.width));
				superUIView.redrawSubviews()
			}
			superUIView.redrawSubviews();
		case .display_z:
			if false && moved
			{
				// doesn't matter where . . .
				calculator.stack().revertToCheckPoint();
				superUIView.animate(100, endPoint: Int(bounds.width));
				superUIView.redrawSubviews()
			}
			superUIView.redrawSubviews();
		case .display_t:
			if false && moved
			{
				// doesn't matter where . . .
				calculator.stack().revertToCheckPoint();
				superUIView.animate(100, endPoint: Int(bounds.width));
				superUIView.redrawSubviews()
			}
			superUIView.redrawSubviews();
		default:
			fatalError();
		}
		
		self.setNeedsDisplay();
	}
	
	open override func moderatedTouchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = false;
		setNeedsDisplay();
	}
}
