//
//  UIColorExtensions.swift
//  |RPN|
//
//  Created by Bradley Smith on 1/21/16.
//  Copyright © 2016 Bradley Smith, LLC. All rights reserved.
//

import UIKit

extension UIColor {
	var invertedColor: UIColor
	{
		get
		{
			return UIColor(red: 1.0 - nativeRed, green: 1.0 - nativeGreen, blue: 1.0 - nativeBlue, alpha: nativeAlpha);
		}
	}
	
	func rgb() -> Int? {
		var fRed : CGFloat = 0
		var fGreen : CGFloat = 0
		var fBlue : CGFloat = 0
		var fAlpha: CGFloat = 0
		if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
			let iRed = Int(fRed * 255.0)
			let iGreen = Int(fGreen * 255.0)
			let iBlue = Int(fBlue * 255.0)
			let iAlpha = Int(fAlpha * 255.0)
			
			//  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
			let rgb = (iAlpha << 24) + (iRed << 16) + (iGreen << 8) + iBlue
			return rgb
		} else {
			// Could not extract RGBA components:
			return nil
		}
	}
	
	var nativeAlpha: CGFloat
	{
		get
		{
			var fRed : CGFloat = 0
			var fGreen : CGFloat = 0
			var fBlue : CGFloat = 0
			var fAlpha: CGFloat = 0
			if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
				return fAlpha;
			} else {
				// Could not extract RGBA components:
				return 0.0
			}
		}
	}
	
	var alphaInt: Int
	{
		get
		{
			return Int(nativeAlpha * 255.0);
		}
	}
	
	var nativeRed: CGFloat
	{
		get
		{
			var fRed : CGFloat = 0
			var fGreen : CGFloat = 0
			var fBlue : CGFloat = 0
			var fAlpha: CGFloat = 0
			if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
				return fRed;
			} else {
				// Could not extract RGBA components:
				return 0.0;
			}
		}
	}
	
	var redInt: Int
	{
		get
		{
			return Int(nativeRed * 255.0)
		}
	}
	
	var nativeGreen: CGFloat
	{
		get
		{
			var fRed : CGFloat = 0
			var fGreen : CGFloat = 0
			var fBlue : CGFloat = 0
			var fAlpha: CGFloat = 0
			if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
				return fGreen
			} else {
				// Could not extract RGBA components:
				return 0;
			}
		}
	}
	
	var greenInt: Int
	{
		get
		{
			return Int(nativeGreen * 255.0)
		}
	}

	var nativeBlue: CGFloat
	{
		get
		{
			var fRed : CGFloat = 0
			var fGreen : CGFloat = 0
			var fBlue : CGFloat = 0
			var fAlpha: CGFloat = 0

			if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
				return fBlue;
			} else {
				// Could not extract RGBA components:
				return 0.0
			}
		}
	}
	
	var blueInt: Int
	{
		get
		{
			return Int(nativeBlue * 255.0);
		}
	}
}
