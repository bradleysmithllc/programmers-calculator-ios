//
//  AppDelegate.swift
//  Programmable Calculator
//
//  Created by Bradley Smith on 11/6/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import programmers_calculator_core

open class Entrails2
{
	let _calculator: Calculator = CalculatorCore();
	let _configuration: Configuration;
	
	fileprivate init()
	{
		_configuration = Configuration(calculator: _calculator);
	}
	
	fileprivate static var entrails: Entrails2?;
	
	public static func initSingleton()
	{
		entrails = Entrails2();
	}
	
	public static var calculator: Calculator
	{
		get
		{
			return entrails!._calculator;
		}
	}
	
	public static var configuration: Configuration
	{
		get
		{
			return entrails!._configuration;
		}
	}
}
