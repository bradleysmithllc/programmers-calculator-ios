//
//  DefaultProfile.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

import programmers_calculator_core

open class JSONProfile: BaseDescribed, Profile
{
	fileprivate var profileData: [Layer: [String: [String: ProfileData]]] = [Layer: [String: [String: ProfileData]]]();

	fileprivate var colors: [String: ProfileData] = [String: ProfileData]();
	fileprivate var fonts: [String: ProfileData] = [String: ProfileData]();

	fileprivate var profileDefaults: [String: ProfileData] = [String: ProfileData]();

	fileprivate static let systemFont: UIFont = UIFont.systemFont(ofSize: 1.0);

	public init()
	{
		super.init(id: "id", description: "description", name: "name");
	}

	public init(id: String, url: URL)
	{
		let data = try! Data(contentsOf: url)
		
		let json = try! JSON(data: data);
		let description = json["description"].string;
		let name = json["name"].string;
		
		super.init(id: id, description: description!, name: name!);

		// pull the colors out
		for (colorName, colorValue) in json["details"]["colors"]
		{
			colors[colorName] = JSONProfile.uiColor(colorValue);
		}

		// make built-in colors available
		colors["#black"] = ProfileData(color: UIColor.black);
		colors["#darkGray"] = ProfileData(color: UIColor.darkGray);
		colors["#lightGray"] = ProfileData(color: UIColor.lightGray);
		colors["#white"] = ProfileData(color: UIColor.white);
		colors["#gray"] = ProfileData(color: UIColor.gray);
		colors["#red"] = ProfileData(color: UIColor.red);
		colors["#green"] = ProfileData(color: UIColor.green);
		colors["#blue"] = ProfileData(color: UIColor.blue);
		colors["#cyan"] = ProfileData(color: UIColor.cyan);
		colors["#yellow"] = ProfileData(color: UIColor.yellow);
		colors["#magenta"] = ProfileData(color: UIColor.magenta);
		colors["#orange"] = ProfileData(color: UIColor.orange);
		colors["#purple"] = ProfileData(color: UIColor.purple);
		colors["#brown"] = ProfileData(color: UIColor.brown);
		colors["#clear"] = ProfileData(color: UIColor.clear);
	
		// pull the fonts out
		for (fontName, fontValue) in json["details"]["fonts"]
		{
			fonts[fontName] = JSONProfile.uiFont(fontValue);
		}

		// pull out the defaults
		for (selectorName, selectorValue) in json["defaults"]
		{
			profileDefaults[selectorName] = uiReference(selectorName, selectorValue: selectorValue.string!);
		}

		for (layerName, layer) in json["profile"]
		{
			let layerIns: Layer = Layer.mapValues[layerName]!;
			
			profileData[layerIns] = [String: [String: ProfileData]]();

			// next level is button types
			for (buttonTypeName, type) in layer
			{
				profileData[layerIns]![buttonTypeName] = [String: ProfileData]();

				for (selectorName, selectorValue) in type
				{
					profileData[layerIns]![buttonTypeName]![selectorName] = uiReference(selectorName, selectorValue: selectorValue.string!);
				}
			}
		}
	}

	fileprivate func color(
		layer: Layer,
		buttonType: String,
		selector: String
		) -> UIColor
	{
		if let data = data(layer: layer, buttonType: buttonType, selector: selector)
		{
			return data.color!;
		}

		return UIColor.black;
	}

	fileprivate func font(
		layer: Layer,
		buttonType: String,
		selector: String
		) -> UIFont
	{
		if let data = data(layer: layer, buttonType: buttonType, selector: selector), let f = data.font
		{
			return f;
		}

		return JSONProfile.systemFont;
	}

	fileprivate func data(
		layer: Layer,
		buttonType: String,
		selector: String
		) -> ProfileData?
	{
		if let l = profileData[layer], let bt = l[buttonType], let d = bt[selector]
		{
			return d;
		}

		// check for a default just for this button type and selector
		if let l = profileDefaults[buttonType + "." + selector]
		{
			return l;
		}

		// check for a default just for this selector
		if let l = profileDefaults[selector]
		{
			return l;
		}

		// check for a default just for this button type
		if let l = profileDefaults[buttonType]
		{
			return l;
		}

		// check for a global default
		if let l = profileDefaults["global"]
		{
			return l;
		}

		// last option - check for this button type and selector under the normal layer
		if let l = profileData[Layer.normal], let bt = l[buttonType], let color = bt[selector]
		{
			return color;
		}

		return nil;
	}
	
	open func buttonColor(buttonSelector buttonType: String, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "buttonColor");
	}
	
	open func buttonPressedColor(buttonSelector buttonType: String, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "buttonPressedColor");
	}
	
	open func lineColor(buttonSelector buttonType: String, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "lineColor");
	}
	
	open func activeMnemonicColor(buttonSelector buttonType: String, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "activeMnemonicColor");
	}
	
	open func activeMnemonicColorPressed(buttonSelector buttonType: String, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "activeMnemonicColorPressed");
	}

	open func inactiveMnemonicColor(buttonSelector buttonType: String, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonType, selector: "inactiveMnemonicColor");
	}
	
	open func inactiveMnemonicColorPressed(buttonSelector: String, layer: Layer) -> UIColor
	{
		return color(layer: layer, buttonType: buttonSelector, selector: "inactiveMnemonicColorPressed");
	}
	
	fileprivate static func uiFont(_ selectorValue: JSON) -> ProfileData
	{
		if let n = selectorValue["style"].string {
			if (n == "italic")
			{
				return ProfileData(font: UIFont.italicSystemFont(ofSize: 1.0));
			}
			else if (n == "bold")
			{
				return ProfileData(font: UIFont.boldSystemFont(ofSize: 1.0));
			}
			else if (n == "normal")
			{
				return ProfileData(font: UIFont.systemFont(ofSize: 1.0));
			}
			else if (n == "light")
			{
                return ProfileData(font: UIFont.monospacedDigitSystemFont(ofSize: 1.0, weight: UIFont.Weight.light));
			}
			else if (n == "ultraLight")
			{
				return ProfileData(font: UIFont.monospacedDigitSystemFont(ofSize: 1.0, weight: UIFont.Weight.ultraLight));
			}
		}
		
		return ProfileData(font: UIFont.systemFont(ofSize: 1.0));
	}
	
	fileprivate func uiReference(_ selectorName: String, selectorValue: String) -> ProfileData
	{
		// use the word color as the marker for colors and fonts
		if (selectorName.contains("Font"))
		{
			return fonts[selectorValue]!;
		}
		else
		{
			return colors[selectorValue]!;
		}
	}

	fileprivate static func uiColor(_ selectorValue: JSON) -> ProfileData
	{
		let redFl: CGFloat;
		let greenFl: CGFloat;
		let blueFl: CGFloat;
		let alphaFl: CGFloat;
		
		// get R, G, B, and A
		if let n: Double = selectorValue["red"].double {
			redFl = CGFloat(n / 255.0);
		}
		else
		{
			redFl = CGFloat(0.0);
		}
		
		if let n: Double = selectorValue["green"].double {
			greenFl = CGFloat(n / 255.0)
		}
		else
		{
			greenFl = CGFloat(0.0);
		}
		
		if let n: Double = selectorValue["blue"].double {
			blueFl = CGFloat(n / 255.0)
		}
		else
		{
			blueFl = CGFloat(0.0);
		}
		
		if let n: Double = selectorValue["alpha"].double {
			alphaFl = CGFloat(n)
		}
		else
		{
			alphaFl = CGFloat(0.0);
		}

		return ProfileData(color: UIColor(red: redFl, green: greenFl, blue: blueFl, alpha: alphaFl));
	}

	open func activeMnemonicFont(buttonSelector buttonType: String, layer: Layer) -> UIFont
	{
		return font(layer: layer, buttonType: buttonType, selector: "activeMnemonicFont");
	}

	open func inactiveMnemonicFont(buttonSelector buttonType: String, layer: Layer) -> UIFont
	{
		return font(layer: layer, buttonType: buttonType, selector: "inactiveMnemonicFont");
	}
}

class ProfileData
{
	let font: UIFont?;
	let color: UIColor?;
	
	init(color: UIColor)
	{
		font = nil;
		self.color = color;
	}
	
	init(font: UIFont)
	{
		self.font = font;
		color = nil;
	}
}
