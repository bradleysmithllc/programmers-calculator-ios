//
//  CalculatorGridView.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/23/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit;

import programmers_calculator_core

open class CalculatorCellButtonFunctionUIView: CalculatorCellUIView
{
	var touching: Bool = false;
	var touchingStartX: CGFloat = 0.0;
	
	fileprivate var attributedStringCache = [String: (attributedString: NSAttributedString, boundsRect: CGRect)]();
	
	fileprivate static var checkCount: Int = 0;
	
	override var cellAllowsInteractionDuringRunningOperation: Bool
	{
		get
		{
			switch calculatorCell.uiFunctionType(activeLayer)
			{
			case .display_x:
				return false;
			case .display_y:
				return false;
			case .display_z:
				return false;
			case .display_t:
				return false;
			case .normal:
				return true;
			case .settings:
				return false;
			case .function_a:
				return true;
			case .function_b:
				return true;
			case .function_c:
				return true;
			case .function_d:
				return true;
			case .placeholder:
				return false;
			}
		}
	};

	override func drawInContext(rect: CGRect, context: CGContext)
	{
		let conf = configuration;
		
		drawCellBackground(
			context,
			cellRect: rect,
			cell: calculatorCell,
			touching: touching,
			operation: calculatorCell.operation(conf.activeLayer)
		);

		drawCellBorder(context: context, cellRect: rect, cell: calculatorCell, touching: touching, operation: calculatorCell.operation(conf.activeLayer));
		
		drawCellText(context: context, cellRect: rect, cell: calculatorCell, touching: touching, operation: calculatorCell.operation(activeLayer));
	}
	
	open override func moderatedTouchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = true;
		
		if let x = touches.first?.location(in: self).x
		{
			touchingStartX = x;
		}
		
		setNeedsDisplay();
	}
	
	open override func moderatedTouchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if let x = touches.first?.location(in: self).x
		{
			superUIView.displayOffset = max(touchingStartX - x, 0.0);
		}
		
		//superUIView.redrawDisplays();
		setNeedsDisplay();
	}
	
	open override func moderatedTouchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = false;
		
		let layer = activeLayer;

		switch (calculatorCell.uiFunctionType(layer))
		{
		case .settings:
			// switch the active layer
			superUIView.settings();
			// check for a function key.  These do not get passed to the calculator engine
		case .normal:
			// switch the active layer
			configuration.activeLayer = Layer.normal;
			superUIView.redrawSubviews()
		case .function_a:
			// switch the active layer
			configuration.activeLayer = Layer.function_a;
			superUIView.redrawSubviews()
		case .function_b:
			// switch the active layer
			configuration.activeLayer = Layer.function_b;
			superUIView.redrawSubviews()
		case .function_c:
			// switch the active layer
			configuration.activeLayer = Layer.function_c;
			superUIView.redrawSubviews()
		case .function_d:
			// switch the active layer
			configuration.activeLayer = Layer.function_d;
			superUIView.redrawSubviews()
		case .placeholder:
			break;
			// ignore
		default:
			fatalError();
		}
		
		self.setNeedsDisplay();
	}
	
	open override func moderatedTouchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		touching = false;
		setNeedsDisplay();
	}
}
