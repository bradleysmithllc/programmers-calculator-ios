//
//  Viewer.swift
//  Notcher
//
//  Created by Bradley Smith on 12/19/17.
//  Copyright © 2017 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class NotchUIView : UIView
{
	var iOS11: Bool {
		get {
			if #available(iOS 11, *) {
				return true;
			}
			else {
				return false
			}
		}
	}
	
	var iPhoneX: Bool {
		get {
			if UIDevice().userInterfaceIdiom == .phone {
				if (UIScreen.main.nativeBounds.height == 2436) {
					return true
				}
			}
			
			return false
		}
	}
	
	var notchSize: CGFloat {
		get {
			return 35
		}
	}
	
	var bottomSize: CGFloat {
		get {
			return 22
		}
	}
	
	var backgroundRect: CGRect {
		get {
			return bounds
		}
	}

	var lastUserRect: CGRect? = nil

	var userRect: CGRect {
		get {
			if (iPhoneX) {
				// determine orientation
				switch (UIDevice.current.orientation)
				{
				case .portrait:
					lastUserRect = CGRect(x: 0, y: notchSize, width: backgroundRect.width, height: backgroundRect.height - notchSize - bottomSize)
				case .portraitUpsideDown:
					lastUserRect = CGRect(x: 0, y: notchSize, width: backgroundRect.width, height: backgroundRect.height - notchSize)
				case .landscapeLeft:
					lastUserRect = CGRect(x: notchSize, y: 0, width: backgroundRect.width - notchSize, height: backgroundRect.height - bottomSize)
				case .landscapeRight:
					lastUserRect = CGRect(x: 0, y: 0, width: backgroundRect.width - notchSize, height: backgroundRect.height - bottomSize)
				case  .unknown, .faceUp, .faceDown:
					break
                @unknown default:
                    fatalError();
                }
			}

			if let lur = lastUserRect {
				return lur
			}
			else {
				return backgroundRect
			}
		}
	}
	
	public final override func draw(_ srect: CGRect)
	{
		guard let context = UIGraphicsGetCurrentContext() else {
			return;
		}
		
		drawBackground(rect: srect, context: context)
	}
	
	public func drawBackground(rect: CGRect, context: CGContext) {}
	public func drawUser(rect: CGRect, context: CGContext) {}
}
