//
//  CalculatorGridView.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/23/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit;

import programmers_calculator_core

open class CalculatorCellOperationUIView: CalculatorCellUIView
{
	private static let operationQueue = DispatchQueue(label: "OperationQueue");
	
	open var operation: programmers_calculator_core.Operation?
	{
		get
		{
			return calculatorCell.operation(activeLayer);
		}
	}

	var touchingMe: Bool
	{
		if let op = operation
		{
			return superUIView.touchingMe(cellUIView: op.id())
		}
		
		return false;
	}
	
	open var needsRefresh: Bool
	{
		get
		{
			if let op = operation, let lm = cellText, let lvs = lastViewState
			{
				if op.mnemonic() == lm && lvs == op.validateStack()
				{
					return false;
				}
			}

			return true;
		}
	}

	var lastViewState: Bool? = nil;

	fileprivate static var checkCount: Int = 0;
	
	override func drawInContext(rect: CGRect, context: CGContext)
	{
		lastViewState = operation?.validateStack();

		let conf = configuration;
		
		drawCellBackground(
			context,
			cellRect: rect,
			cell: calculatorCell,
			touching: touchingMe,
			operation: calculatorCell.operation(conf.activeLayer)
		);
		
		drawCellBorder(context: context, cellRect: rect, cell: calculatorCell, touching: touchingMe, operation: calculatorCell.operation(conf.activeLayer));
		
		drawCellText(context: context, cellRect: rect, cell: calculatorCell, touching: touchingMe, operation: calculatorCell.operation(activeLayer));
	}

	open override func moderatedTouchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (!touchingMe)
		{
			if let op = calculatorCell.operation(activeLayer)
			{
				if (op.validateStack())
				{
					superUIView.touch(cellUIView: op.id());
					
					setNeedsDisplay();
				}
			}
		}
	}
	
	open override func moderatedTouchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		superUIView.displayOffset = 0.0;
		
		if (touchingMe)
		{
			let layer = activeLayer;

			if let op = calculatorCell.operation(layer)
			{
				// perform this asynchronously
				CalculatorCellOperationUIView.operationQueue.async {
					self.superUIView.beginOperation();
					self.calculator.stack().checkPoint();
					self.activeLayout.enforceConsistency(self.calculator);
					
					do
					{
						try self.calculator.invoke(op.id());
					}
					catch
					{
						print("\(error)");
					}

					DispatchQueue.main.async(execute: {
						// make sure to update the UI to refelect possibly changed state
						self.superUIView.completeOperation();
						self.superUIView.untouch();
						self.superUIView.redrawSubviews();
					})
				}
			}
		}
	}
	
	open override func moderatedTouchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
	{
		if (touchingMe)
		{
			superUIView.untouch();
		}

		setNeedsDisplay();
	}
}
