//
//  ViewController.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import UIKit

class StackViewController: UIViewController
{
	@IBOutlet var stackTableView: UITableView!;
	var stackTableDataSource: StackTableDataSource?;

	override func viewDidLoad()
	{
		super.viewDidLoad()

		stackTableDataSource = StackTableDataSource();

		stackTableView.dataSource = stackTableDataSource;
		stackTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
}

class StackTableDataSource: NSObject, UITableViewDataSource
{
	@objc func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let rowCount = max(4, AppDelegate.calculator.stack().size());
		
		return rowCount;
	}
	
	@objc func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!;

		var rowText = "";

		let row = indexPath.row;

		if (row == 0)
		{
			let xe = AppDelegate.calculator.stack().getXElement();

			rowText = "X : " + (xe.isMessage ? xe.message() : CalculatorCellOperationUIView.displayNumber(xe.number(), calculator: AppDelegate.calculator));
		}
		else if (row == 1)
		{
			let ye = AppDelegate.calculator.stack().getYElement();
			
			rowText = "Y : " + (ye.isMessage ? ye.message() : CalculatorCellOperationUIView.displayNumber(ye.number(), calculator: AppDelegate.calculator));
		}
		else if (row == 2)
		{
			let ye = AppDelegate.calculator.stack().getZElement();
			
			rowText = "Z : " + (ye.isMessage ? ye.message() : CalculatorCellOperationUIView.displayNumber(ye.number(), calculator: AppDelegate.calculator));
		}
		else if (row == 3)
		{
			let ye = AppDelegate.calculator.stack().getTElement();
			
			rowText = "T : " + (ye.isMessage ? ye.message() : CalculatorCellOperationUIView.displayNumber(ye.number(), calculator: AppDelegate.calculator));
		}
		else
		{
			rowText = "-";

			if (row < 13)
			{
				rowText += " " + String(row - 3);
			}
			else
			{
				rowText += String(row - 3);
			}
			
			let re = AppDelegate.calculator.stack().peekElement(AppDelegate.calculator.stack().size() - row - 1);
			
			rowText += ": " + (re.isMessage ? re.message() : CalculatorCellOperationUIView.displayNumber(re.number(), calculator: AppDelegate.calculator));
		}

		cell.textLabel?.text = rowText;

		return cell;
	}

	private func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
	}
}
