
//
//  Configuration.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

import programmers_calculator_core
import Device

protocol OrientationListener
{
	func orientationChanged(_ o: Orientation);
	func id() -> String;
}

open class Configuration
{
	static let _debugGraphics = false;
	
	fileprivate var orientationListeners: [String: OrientationListener] = [String: OrientationListener]();

	fileprivate let _defaultProfile: Profile;

	fileprivate var _activeProfile: Profile?;

	fileprivate let resourceRoot = Bundle(for: Configuration.self).resourceURL!.appendingPathComponent("CalculatorResources");
	
	fileprivate let perspectiveRoot: URL;
	fileprivate let profileRoot: URL;
	fileprivate let compoundOperationsRoot: URL;

	fileprivate let _defaultLandscapePerspectiveName: String;
	fileprivate let _defaultPortraitPerspectiveName: String;

	fileprivate var _activeLandscapePerspective: Layout?;
	fileprivate var _activePortraitPerspective: Layout?;
	
	fileprivate var _perspectives: [String: Layout] = [String: Layout]();
	fileprivate let _perspectivePaths: [String: URL];
	fileprivate let _perspectiveNames: [String];

	fileprivate let _profiles: [String: Profile];
	fileprivate let _profileKeys: [String];

	fileprivate let _profileLayout: String = "_profile_cell";

	fileprivate var landscape: Bool = false;
	
	fileprivate let calculator: Calculator;

	fileprivate var _activePortraitLayer: Layer = Layer.normal;
	fileprivate var _activeLandscapeLayer: Layer = Layer.normal;

	public init(calculator: Calculator)
	{
		print("Bounds: {height: \(UIScreen.main.nativeBounds.height), width: \(UIScreen.main.nativeBounds.width)}");
		print("Area: \(UIScreen.main.nativeBounds.height * UIScreen.main.nativeBounds.width)");
		print("Aspect: \(UIScreen.main.nativeBounds.width / UIScreen.main.nativeBounds.height)");

		self.calculator = calculator;

		var pers = [String: URL]();
		var persIds = [String]();

		perspectiveRoot = Configuration.urlAtPath(["Layouts"], root: resourceRoot);
		
		// perspectives
		do
		{
			let pathItems = try FileManager.default.contentsOfDirectory(at: perspectiveRoot, includingPropertiesForKeys: nil, options: [.skipsSubdirectoryDescendants, .skipsPackageDescendants, .skipsHiddenFiles]);
			
			for pathItem in pathItems
			{
				if (pathItem.pathExtension == "json")
				{
					let filename: String = pathItem.deletingPathExtension().lastPathComponent;
					//let perspective = JSONLayout(id: filename, url: pathItem);
					
					pers[filename] = pathItem;

					if !filename.hasPrefix("_")
					{
						persIds.append(filename);
					}
				}
			}
		}
		catch
		{
		}

		_perspectivePaths = pers;
		_perspectiveNames = persIds.sorted() { $1 > $0 };
		
		_defaultLandscapePerspectiveName = "X Scientific";
		_defaultPortraitPerspectiveName = "X Basic";

		// profiles
		profileRoot = Configuration.urlAtPath(["Profiles"], root: resourceRoot);
		
		var profs = [String: Profile]();
		
		do
		{
			let pathItems = try FileManager.default.contentsOfDirectory(at: profileRoot, includingPropertiesForKeys: nil, options: [.skipsSubdirectoryDescendants, .skipsPackageDescendants, .skipsHiddenFiles]);
			
			for pathItem in pathItems
			{
				let filename: String = pathItem.deletingPathExtension().lastPathComponent;
				let profile = JSONProfile(id: filename, url: pathItem);
				
				profs[filename] = profile;
			}
		}
		catch
		{
		}

		_profiles = profs;
		_profileKeys = Array(_profiles.keys).sorted() { $1 > $0 };

		_defaultProfile = _profiles["Blue OS"]!;

		// compound operations
		compoundOperationsRoot = Configuration.urlAtPath(["CompoundOperations"], root: resourceRoot);
		
		do
		{
			let pathItems = try FileManager.default.contentsOfDirectory(at: compoundOperationsRoot, includingPropertiesForKeys: nil, options: [.skipsSubdirectoryDescendants, .skipsPackageDescendants, .skipsHiddenFiles]);
			
			for pathItem in pathItems
			{
				let filename: String = pathItem.deletingPathExtension().lastPathComponent;
				let operation = JSONCompoundOperation(calculator: calculator, id: filename, url: pathItem);

				calculator.addOperation(operation);
			}
		}
		catch
		{
		}
		
		// one day determine which perspective or profile is active
		var val: String? = getUserDefault("Profile");

		if let v = val
		{
			if let _ = _profiles[v]
			{
			}
			else
			{
				val = _defaultProfile.id();
			}
		}
		
		activateProfile(val != nil ? val! : _defaultProfile.id());
		
		val = getUserDefault("LandscapeLayout");

		if let v = val
		{
			if _perspectiveNames.contains(v)
			{
			}
			else
			{
				val = _defaultLandscapePerspectiveName;
			}
		}
		else
		{
			val = _defaultLandscapePerspectiveName;
		}

		activateLandscapeLayout(val!);
		
		val = getUserDefault("PortraitLayout");

		
		if let v = val
		{
			if _perspectiveNames.contains(v)
			{
			}
			else
			{
				val = _defaultPortraitPerspectiveName;
			}
		}
		else
		{
			val = _defaultPortraitPerspectiveName;
		}

		activatePortraitLayout(val!);
	}

	func addOrientationListener(_ ol: OrientationListener)
	{
		orientationListeners[ol.id()] = ol;
	}

	func removeOrientationListener(_ ol: OrientationListener)
	{
		orientationListeners[ol.id()] = nil;
	}

	open func landscape(_ l: Bool)
	{
		landscape = l;
		
		// broadcast change to listeners
		for (_, ol) in orientationListeners
		{
			ol.orientationChanged(orientation())
		}
	}
	
	open func isLandscape() -> Bool
	{
		return landscape;
	}
	
	open func orientation() -> Orientation
	{
		return landscape ? .landscape : .portrait;
	}

	open func profiles() -> [String]
	{
		return _profileKeys;
	}

	open func profile(_ id: String) -> Profile
	{
		return _profiles[id]!;
	}

	open func layouts() -> [String]
	{
		return _perspectiveNames;
	}
	
	open func layout(_ id: String) -> Layout!
	{
		if let p = _perspectives[id]
		{
			return p;
		}
		else if let p = _perspectivePaths[id]
		{
			let jl = JSONLayoutLoader.loadFromJSON(id, url: p)!;
			
			_perspectives[id] = jl;

			jl.populateOperations(calculator);

			return _perspectives[id];
		}
		else
		{
			let dpname = isLandscape() ? _defaultLandscapePerspectiveName : _defaultPortraitPerspectiveName;
			
			if let p = _perspectives[dpname]
			{
				return p;
			}

			_perspectives[dpname] = JSONLayoutLoader.loadFromJSON(dpname, url: _perspectivePaths[dpname]!)!;
			
			return _perspectives[dpname];
		}
	}

	open func activeLandscapeLayout() -> Layout!
	{
		return _activeLandscapePerspective;
	}
	
	open func activePortraitLayout() -> Layout!
	{
		return _activePortraitPerspective;
	}

	fileprivate func userDefault(_ id: String, value: String)
	{
		let defaults = UserDefaults.standard;

		defaults.setValue(value, forKey: id);
	}

	fileprivate func getUserDefault(_ id: String) -> String?
	{
		let defaults = UserDefaults.standard;
		
		return defaults.string(forKey: id);
	}
	
	open func activateLayout(_ id: String)
	{
		// ensure that out current numerical base is supported
		let p = layout(id);

		p?.enforceConsistency(calculator);

		if (landscape)
		{
			activateLandscapeLayout(id);
		}
		else
		{
			activatePortraitLayout(id);
		}
	}

	fileprivate func activateLandscapeLayout(_ id: String)
	{
		_activeLandscapePerspective = layout(id);

		activeLayer = Layer.normal;

		userDefault("LandscapeLayout", value: id);
	}
	
	fileprivate func activatePortraitLayout(_ id: String)
	{
		_activePortraitPerspective = layout(id);
		userDefault("PortraitLayout", value: id);
	}

	open func activateProfile(_ id: String)
	{
		_activeProfile = profile(id);
		userDefault("Profile", value: id);
	}

	open var activeProfile: Profile
	{
		get
		{
			return _activeProfile!;
		}
	}
	
	open var activeLayout: Layout
	{
		get
		{
			if (landscape)
			{
				return _activeLandscapePerspective!;
			}
			else
			{
				return _activePortraitPerspective!;
			}
		}
	}
	
	public static func urlAtPath(_ path: [String], root: URL) -> URL
	{
		var url = root;
		
		for pathElement in path
		{
			url = url.appendingPathComponent(pathElement);
		}
		
		return url;
	}
	
	open func urlAtPath(_ path: [String]) -> URL
	{
		return Configuration.urlAtPath(path, root: resourceRoot);
	}
	
	open var activeLayer: Layer
	{
		get
		{
			// check against active layout to ensure the layer is valid
			let actLayout = activeLayout;
			let layoutLayers = actLayout.layers();

			if (isLandscape())
			{
				if !layoutLayers.contains(_activeLandscapeLayer)
				{
					_activeLandscapeLayer = .normal;
				}

				return _activeLandscapeLayer;
			}
			else
			{
				if !layoutLayers.contains(_activePortraitLayer)
				{
					_activePortraitLayer = .normal;
				}
				
				return _activePortraitLayer;
			}
		}
		set(layer)
		{
			if (isLandscape())
			{
				_activeLandscapeLayer = layer;
			}
			else
			{
				_activePortraitLayer = layer;
			}
		}
	}
}
