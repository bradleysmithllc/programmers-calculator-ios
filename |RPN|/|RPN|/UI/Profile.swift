//
//  Profile.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit

public protocol Profile: Described
{
	func buttonColor(buttonSelector: String, layer: Layer) -> UIColor;
	func buttonPressedColor(buttonSelector: String, layer: Layer) -> UIColor;
	func lineColor(buttonSelector: String, layer: Layer) -> UIColor;

	func activeMnemonicColor(buttonSelector: String, layer: Layer) -> UIColor;
	func activeMnemonicColorPressed(buttonSelector: String, layer: Layer) -> UIColor;

	func inactiveMnemonicColor(buttonSelector: String, layer: Layer) -> UIColor;
	func inactiveMnemonicColorPressed(buttonSelector: String, layer: Layer) -> UIColor;

	func activeMnemonicFont(buttonSelector: String, layer: Layer) -> UIFont;
	func inactiveMnemonicFont(buttonSelector: String, layer: Layer) -> UIFont;
}
