//
//  Perspective.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import SwiftyJSON

import programmers_calculator_core

// really just a placeholder for the static load method
open class JSONLayoutLoader
{
	public static func loadFromJSON(_ id: String, url: URL) -> Layout?
	{
		let data = try! Data(contentsOf: url)
		
		let jsonRoot = try! JSON(data: data);

		let version = jsonRoot["json-layout-version"].string!;

		if version.hasPrefix("JSONTableLayout:")
		{
			return JSONTableLayout(id: id, jsonRoot: jsonRoot);
		}
		else if version.hasPrefix("JSONLayout:")
		{
			return JSONLayout(id: id, jsonRoot: jsonRoot);
		}

		return nil;
	}
}
