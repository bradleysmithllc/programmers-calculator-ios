//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class JSONCompoundOperationTests: TestSuperclass {
	func tesDefaultProfile() {
		let op: JSONCompoundOperation = TestCompoundOperation(calculator: calculator, id: "testCompoundOperation");
		
		XCTAssertEqual("d.ms", op.id());
		XCTAssertEqual("d.ms", op.mnemonic());
		XCTAssertEqual(7, op.operations().count);

		XCTAssertEqual("ent", op.operations()[0]);
		XCTAssertEqual("2", op.operations()[1]);
		XCTAssertEqual(".", op.operations()[2]);
		XCTAssertEqual("7", op.operations()[3]);
		XCTAssertEqual("5", op.operations()[4]);
		XCTAssertEqual("2#.75", op.operations()[5]);
		XCTAssertEqual("store", op.operations()[6]);
	}
}