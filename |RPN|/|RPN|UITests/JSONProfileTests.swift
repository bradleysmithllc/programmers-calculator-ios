//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import _RPN_

class JSONProfileTests: XCTestCase {
	func tesDefaultProfile() {
		let profile: JSONProfile = TestProfile(id: "");
		
		let _ = profile.lineColor(buttonSelector: OperationType.literal.rawValue, layer: Layer.function_a);
		let _ = UIColor.black;

		//XCTAssertEqual(x, clr);
		let _ = profile.buttonColor(buttonSelector: OperationType.literal.rawValue, layer: Layer.normal);
	}
	
	func testInit() {
		let profile: JSONProfile = TestProfile(id: "testProfile");
		XCTAssertEqual(UIColor.black, profile.lineColor(buttonSelector: OperationType.literal.rawValue, layer: Layer.function_a));
		XCTAssertEqual(UIColor.white, profile.buttonColor(buttonSelector: "operation_b", layer: Layer.normal));
		XCTAssertEqual(UIColor.black, profile.buttonPressedColor(buttonSelector: "operation_b", layer: Layer.normal));
	}
	
	func testEmptyProfile() {
		let profile: JSONProfile = TestProfile(id: "missingProfile");
		XCTAssertEqual(UIColor.black, profile.lineColor(buttonSelector: OperationType.literal.rawValue, layer: Layer.function_a));
	}

	func testDefaults() {
		let profile: JSONProfile = TestProfile(id: "testProfileDefaults");
		XCTAssertEqual(100, profile.lineColor(buttonSelector: OperationType.literal.rawValue, layer: Layer.function_a).redInt);
		XCTAssertEqual(100, profile.lineColor(buttonSelector: OperationType.literal.rawValue, layer: Layer.normal).redInt);
		XCTAssertEqual(101, profile.buttonPressedColor(buttonSelector: OperationType.literal.rawValue, layer: Layer.normal).redInt);
	}

	override func setUp() {
		super.setUp()
		UIApplication.shared.delegate = TestAppDelegate()
	}
}
