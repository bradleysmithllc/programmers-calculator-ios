//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class HelpControllerTests: TestSuperclass {
	func testEmpty() {
		XCTAssertEqual("", HelpViewController.processTags(""));
	}

	func testYX() {
		XCTAssertEqual("y<<x", HelpViewController.processTags("y<<x"));
	}
}
