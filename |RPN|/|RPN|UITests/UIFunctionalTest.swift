//
//  UIFunctionalTest.swift
//  |RPN|UITests
//
//  Created by absmiths on 7/7/20.
//  Copyright © 2020 Bradley Smith, LLC. All rights reserved.
//

import XCTest

class UIFunctionalTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
			let app = XCUIApplication();
			app.launch();
			app.launchArguments += ["-AppleLanguages", "(en)"]
			app.launchArguments += ["-AppleLocale", "en_US"]

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
						        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
}
