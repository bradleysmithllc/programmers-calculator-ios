//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class BinaryTests: TestSuperclass {
	func testNor() {
		stack.setNumericBehavior(.UnsignedInteger);
		stack.setIntegerWordSize(.byte);
		stack.setNumericBase(base: 0x10);

		calculator.stack().push(makeNumber(integer: 4));

		do {
			try calculator.invoke("nor");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual("FB", CalculatorCellOperationUIView.displayNumber(calculator.stack().peek(), calculator: calculator));
		} catch _ {
			XCTFail();
		}
	}
}
