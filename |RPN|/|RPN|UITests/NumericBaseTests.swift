//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class NumericBaseTests: TestSuperclass {
	func testHex2() {
		XCTAssertEqual("29D", CalculatorCellOperationUIView.displayNumber(669.13333333, numericBehavior: .Decimal, integerWordSize: nil, numericBase: 16, decimals: 5));
	}
	
	func testBug() {
		do
		{
			try calculator.invoke("dec");
			try calculator.invoke("1");
			try calculator.invoke("00");
			try calculator.invoke("3");
			try calculator.invoke(".");
			try calculator.invoke("00");
			try calculator.invoke("7");
			try calculator.invoke("ent");
			try calculator.invoke("7");
			try calculator.invoke("/");
			XCTAssertEqual(NSDecimalNumber(value: 143.28671 as Double).decimalNumberRoundedToDigits(5), stack.peek().decimalNumberRoundedToDigits(5));

			try calculator.invoke("hex");

			XCTAssertEqual("8F", CalculatorCellOperationUIView.displayNumber(stack.pop(), numericBehavior: .Decimal, integerWordSize: nil, numericBase: 16, decimals: 5));
		}
		catch
		{
			XCTFail();
		}
	}
}
